import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { CreatePostDto } from './dto/createPost.dto';
import { UpdatePostDto } from './dto/updatePost.dto';
import { PostsService } from './posts.service';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Post()
  @ApiResponse({
    status: HttpStatus.CREATED,
  })
  create(@Body() createPostDto: CreatePostDto) {
    return this.postsService.create(createPostDto);
  }

  @Get()
  @ApiResponse({
    status: HttpStatus.OK,
  })
  findAll() {
    return this.postsService.findAll();
  }

  @Get(':id')
  @ApiResponse({
    status: HttpStatus.OK,
  })
  findOne(@Param('id') id: string) {
    return this.postsService.findOne(id);
  }

  @Put(':id')
  @ApiResponse({
    status: HttpStatus.ACCEPTED,
  })
  update(@Param('id') id: string, @Body() updatePostDto: UpdatePostDto) {
    return this.postsService.update(id, updatePostDto);
  }

  @Delete(':id')
  @ApiResponse({
    status: HttpStatus.ACCEPTED,
  })
  remove(@Param('id') id: string) {
    return this.postsService.delete(id);
  }
}
