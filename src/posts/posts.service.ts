import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Post, PostDocument } from './schemas/post.schema';
import { CreatePostDto } from './dto/createPost.dto';
import { throwError } from 'rxjs';

@Injectable()
export class PostsService {
  constructor(@InjectModel(Post.name) private postModel: Model<PostDocument>) {}

  async create(createCatDto: CreatePostDto): Promise<string> {
    try {
      const createdPost = new this.postModel(createCatDto);
      await createdPost.save();
      return 'Пост создан!';
    } catch (error) {
      throwError(new Error(error.message));
    }
  }

  async findAll(): Promise<Post[] | string> {
    try {
      const posts = await this.postModel.find().exec();
      return posts || 'Посты не найдены!';
    } catch (error) {
      throwError(new Error(error.message));
    }
  }

  async findOne(id: string): Promise<Post | string> {
    try {
      const post = await this.postModel.findById(id);
      return post || `Пост с ${id} не найден!`;
    } catch (error) {
      throwError(new Error(error.message));
    }
  }

  async update(id: string, payload: Post): Promise<string> {
    try {
      const post = await this.postModel.findByIdAndUpdate(id, payload);
      return post
        ? `Пост с ${id} обновлен!`
        : 'Неудалось обновить пост, поскольку его не существует!';
    } catch (error) {
      throwError(new Error(error.message));
    }
  }

  async delete(id: string): Promise<string> {
    try {
      const post = await this.postModel.findByIdAndDelete(id);
      return post
        ? `Пост с ${id} УДАЛЕН!`
        : 'Неудалось удалить пост, поскольку его не существует!';
    } catch (error) {
      throwError(new Error(error.message));
    }
  }
}
